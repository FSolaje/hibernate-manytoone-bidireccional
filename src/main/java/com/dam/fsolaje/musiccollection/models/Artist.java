package com.dam.fsolaje.musiccollection.models;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Artist {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    /* mappedBy nos indica que va a ser la clase Artist la que va a guardar la relación
    mediante un columna extra en su tabla.
     */
    @OneToMany(mappedBy = "artist", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Disc> discList = new ArrayList<>();

    public Artist() {
    }

    public Artist(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    /*
     * Los Métodos helper que encontramos a continuación nos van a ayudar
     * a mantener actualizada la relación.
     */
    public void addDisc(Disc disc) {
        discList.add(disc);
        disc.setArtist(this);
    }

    public void removeDisc(Disc disc) {
        discList.remove(disc);
        disc.setArtist(null);
    }

    @Override
    public String toString() {
        return "Artist{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", discList=" + discList +
                '}';
    }
}
