package com.dam.fsolaje.musiccollection.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Disc {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY )
    private long id;
    private String title;
    @ManyToOne(fetch = FetchType.LAZY)
    private Artist artist;

    public Disc(){}
    public Disc(String title, Artist artist){
        this.artist = artist;
        this.title=title;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Artist getArtist() {
        return artist;
    }

    public void setArtist(Artist artist) {
        this.artist = artist;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Disc disc = (Disc) o;
        return title.equals(disc.title) && artist.equals(disc.artist);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, artist);
    }

    @Override
    public String toString() {
        return "Disc{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", artist=" + artist.getName() +
                '}';
    }
}
