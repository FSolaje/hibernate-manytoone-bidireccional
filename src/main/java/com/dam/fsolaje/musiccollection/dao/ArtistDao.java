package com.dam.fsolaje.musiccollection.dao;

import com.dam.fsolaje.musiccollection.models.Artist;
import org.hibernate.Session;

/**
 * Esta clase recoge todos los métodos de acceso para la clase Arstista.
 * Al tratarse de una relación de OneToMany que hemos delegado en Disco,
 * los métodos de acceso son muy concisos.
 * @author Fran Tomás
 */

public class ArtistDao extends Dao<Artist> {
    public ArtistDao(Session session){
        super(session);
    }

    @Override
    protected Class<Artist> getTClass() {
        return Artist.class;
    }

    @Override
    public void create(Artist artist) {
       getSession().save(artist) ;
    }

    @Override
    public void update(Artist artist) {
        getSession().merge(artist);
    }

    @Override
    public void remove(Artist artist) {
        getSession().remove(artist);
    }
}
