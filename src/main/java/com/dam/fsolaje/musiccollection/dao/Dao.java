package com.dam.fsolaje.musiccollection.dao;

import org.hibernate.Session;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Clase abstracta que servira de base para la creación del resto de clases
 * CRUD, en mi caso las sigo denominando DAO.
 *
 * @author Fran Tomás
 */
public abstract class Dao<T> {

    private Session session;

    public Dao(Session session) {
        this.session = session;
    }

    public Session getSession() {
        return session;
    }

    /**
     * Es necesario para obtener la clase T, ya que esta no admite getClass() heredado
     * de object.
     * @return la clase .class correspondiente a T en su implementación.
     */
    protected abstract Class<T> getTClass();

    public abstract void create(T object);

    public abstract void update(T object);

    public abstract void remove(T object);

    /**
     * Este método será común a todas las entidades, ya que las definimos con un id
     * por ese motivo lo vamos a desarrollar de forma genérica.
     *
     * @param id identificador del objeto a obtener.
     * @return Puede devolver null si no se encuentra el objeto en la base de datos.
     */
    public T getById(int id) {
        Class<T> clazz = getTClass();
        T object;

        /*
         * No podemos parametrizar la tabla, por lo que es imposible usar Query para
         * pasar como parámetro la clase a la qeu debe dirigir la consulta.
         * Mediante la API Criteria podemos crear estas consultas dinámicas donde si se puede
         * introducir la clase como parámetro.
         */
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<T> query = cb.createQuery(clazz);
        Root<T> table = query.from(clazz);
        query.select(table);
        query.where(cb.equal(table.get("id"), id));

        List<T> result = session.createQuery(query).list();
        if (result.size() > 0) {
            object = result.get(0);
        } else {
            object = null;
        }

        return object;
    }

    /**
     * Pese a lo que se puede pensar en un principio, no es necesario comenzar la transacción
     * antes de la creación de los datos. Perfectamente podemos ir salvando en session cada uno
     * de los objetos y finalmente persistirlos.
     */
    public void persist() {
        if (!session.getTransaction().isActive()) {
            session.beginTransaction();
        }
        session.getTransaction().commit();
    }
}
