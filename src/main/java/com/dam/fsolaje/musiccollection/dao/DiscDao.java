package com.dam.fsolaje.musiccollection.dao;

import com.dam.fsolaje.musiccollection.models.Disc;
import org.hibernate.Session;

/**
 * Esta clase es la que lleva el peso de la relación pues
 * es la que se encarga de realizar los cambios en la misma cuando
 * se añade, modifica o elimina un disco de la base de datos.
 *
 * @author Fran Tomás
 */
public class DiscDao extends Dao<Disc>{

    public DiscDao(Session session){
        super(session);
    }

    @Override
    public Class<Disc> getTClass() {
        return Disc.class;
    }

    /**
     * A diferencia de la clase ArtistDao, aquí tenemos que
     * actualizar las listas de discos de los artistas cuando creamos
     * un nuevo disco.
     *
     * @param disc nuevo disco que vamos a introducir en la base de datos.
     *
     */
    //TODO filtrar la excepción cuando se pasa un disco sin Artista.
    @Override
    public void create(Disc disc) {
        getSession().save(disc);
        disc.getArtist().addDisc(disc);
    }

    @Override
    public void update(Disc disc) {
        getSession().merge(disc);
    }

    @Override
    public void remove(Disc disc) {
        disc.getArtist().removeDisc(disc);
        getSession().remove(disc);

    }
}
