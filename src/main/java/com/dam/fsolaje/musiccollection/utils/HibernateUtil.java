package com.dam.fsolaje.musiccollection.utils;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import java.util.Optional;

public class HibernateUtil {
    private static SessionFactory instance;

        private HibernateUtil() {
        }

        public static SessionFactory getInstance() {
            return Optional.ofNullable(instance).orElse(instanceSessionFactory());
        }

        private static SessionFactory instanceSessionFactory() {
            StandardServiceRegistry sr = new StandardServiceRegistryBuilder().
                    configure()
                    .build();
            instance = new MetadataSources(sr).buildMetadata().buildSessionFactory();
            return instance;
        }

        public static Session openSession() {
            return getInstance().openSession();

        }

        public static void close() {
            Optional.of(instance).ifPresent(SessionFactory::close);
            instance=null;
        }
}
