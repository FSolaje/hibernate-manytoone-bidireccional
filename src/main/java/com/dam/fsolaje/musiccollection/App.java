package com.dam.fsolaje.musiccollection;

import com.dam.fsolaje.musiccollection.models.Artist;
import com.dam.fsolaje.musiccollection.dao.ArtistDao;
import com.dam.fsolaje.musiccollection.dao.DiscDao;
import com.dam.fsolaje.musiccollection.models.Disc;
import com.dam.fsolaje.musiccollection.utils.HibernateUtil;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Clase principal que lanza el programa.
 *
 * @author Fran Tomás
 */
public class App {
    public static void main(String[] args) {
        Session session = HibernateUtil.openSession();
        ArtistDao artistDao = new ArtistDao(session);
        DiscDao discDao = new DiscDao(session);

        ArrayList<Artist> artistsList = new ArrayList<>(Arrays.asList(
                new Artist("Queen"),
                new Artist("Bettles"),
                new Artist("The OffSpring")
        ));

        // Creamos y persistimos los artistas
        for(Artist artist: artistsList){
            artistDao.create(artist);
        }
        artistDao.persist();


        ArrayList<Disc> discsList = new ArrayList<>(Arrays.asList(
                new Disc("Original Plastar",artistDao.getById(3)),
                new Disc("Yesterday",artistDao.getById(2)),
                new Disc("Boemian Rapsody",artistDao.getById(3))
        ));

        // Creamos y persistimos los discos.
        for(Disc disc: discsList){
            discDao.create(disc);
        }
        discDao.persist();

        session.close();
        HibernateUtil.close();
    }
}
